PLATFORMS="linux/amd64,linux/arm64"
REPO="rileyvel/php-base-docker-containers"
BUILDX_ARGS=--push --no-cache

build: mediawiki mediawiki-fpm

mediawiki: profile-mediawiki/Dockerfile
	docker buildx build \
		--platform ${PLATFORMS} \
		-t ${REPO}:mediawiki \
		${BUILDX_ARGS} \
		./profile-mediawiki

nextcloud: profile-nextcloud/Dockerfile
	docker buildx build \
		--platform ${PLATFORMS} \
		-t ${REPO}:nextcloud \
		${BUILDX_ARGS} \
		./profile-nextcloud

mediawiki-fpm: profile-mediawiki-fpm/Dockerfile
	docker buildx build \
		--platform ${PLATFORMS} \
		-t ${REPO}:mediawiki-fpm \
		${BUILDX_ARGS} \
		./profile-mediawiki-fpm
