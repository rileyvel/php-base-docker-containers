# php-base-docker-containers

Docker containers for my PHP services

## Building

Just log in to docker hub, and run `make build`. 

This will build for all supported platforms and push automatically. 
Be prepared to spend quite a bit of time.
